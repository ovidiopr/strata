//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file Strata/Strata.cc
/// \brief Strata's main program
//
// 
//==============================================================================
//==============================================================================

#include <vector>
#include <string>

#include "G4Types.hh"

#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif

#include "G4UImanager.hh"
#include "Randomize.hh"

#include "DetectorConstruction.hh"
#include "PhysicsList.hh"
#include "PrimaryGeneratorAction.hh"

#include "ActionInitialization.hh"
#include "Run.hh"
#include "SteppingVerbose.hh"

#include "G4UIExecutive.hh"
#include "G4VisExecutive.hh"

//==============================================================================

int main(int argc, char** argv) {
    enum mode_states {readMacros, readThreads};

    std::vector<G4String> macroFiles;
    G4int numThreads = 0;
    G4bool graphicalMode = false;

    std::vector<G4String> args;
    args.assign(argv, argv + argc);

    std::string help_msg("Usage: " + args[0] + " [-h] [-g]" +
#ifdef G4MULTITHREADED
                         " [-n Threads]" +
#endif
                         " [file.mac ...]\n" +
                         "            -h --> Show this message.\n" +
                         "            -g --> Graphical (interactive) mode.\n" +
#ifdef G4MULTITHREADED
                         "            -n --> Define number of threads.\n" +
#endif
                         "                   All other arguments are interpreted as macro files.\n");

    // Now remove first element (exe name)
    args.erase (args.begin());

    G4int mode = readMacros;
    for (const auto& arg : args) {
        // For each arg in args list we detect the change of the current
        // read mode or read the arg. The reading args algorithm works
        // as a finite-state machine.

        // Detecting new read mode (if it is a valid -key)
        if (arg == "-h") {
            std::cout << help_msg << std::endl;
            return 0;
        }

        if (arg == "-g") {
            graphicalMode = true;
            mode = readMacros;
            continue;
        }

#ifdef G4MULTITHREADED
        if (arg == "-n") {
            mode = readThreads;
            continue;
        }

        if (mode == readThreads) {
            numThreads = std::stoi(arg);
            mode = readMacros;
            continue;
        }
#endif

        // If nothing else, then the argument is the name of a macro file
        macroFiles.push_back(arg);
        std::cout << arg << "\n";
    }

    // If no macro name was provided, set an interactive session
    if (macroFiles.empty())
        graphicalMode = true;

    // Detect interactive mode and define UI session
    G4UIExecutive* ui = nullptr;
    if (graphicalMode)
        ui = new G4UIExecutive(argc, argv);

    // Choose the Random engine
    CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);
    //CLHEP::HepRandom::setTheEngine(new CLHEP::MTwistEngine);
    CLHEP::HepRandom::setTheSeed((unsigned)clock());
    //CLHEP::HepRandom::setTheSeed(time(NULL));

    // Verbose output class
    G4VSteppingVerbose::SetInstance(new SteppingVerbose);

    // Construct the default run manager
#ifdef G4MULTITHREADED
    auto* runManager = new G4MTRunManager;
    G4int nThreads = std::min(G4Threading::G4GetNumberOfCores(), 4);
    if (numThreads > 0)
        nThreads = numThreads;

    runManager->SetNumberOfThreads(nThreads);
    G4cout << "===== Strata is started with "
           << runManager->GetNumberOfThreads() << " threads =====" << G4endl;
#else
    auto* runManager = new G4RunManager;
#endif

    // Set mandatory initialization classes
    auto* detector = new DetectorConstruction();
    auto* physics = new PhysicsList();

    runManager->SetUserInitialization(detector);
    runManager->SetUserInitialization(physics);

    // Set user action classes
    runManager->SetUserInitialization(new ActionInitialization(detector));

    // Initialize visualization
    G4VisManager* visManager = nullptr;

    // Get the pointer to the User Interface manager
    G4UImanager* UImanager = G4UImanager::GetUIpointer();

    // Run macros from the 'macros' directory
    UImanager->ApplyCommand("/control/macroPath macros");
    if (ui) {
        // Interactive mode
        visManager = new G4VisExecutive;
        visManager->Initialize();
        if (macroFiles.empty()) {
            UImanager->ApplyCommand("/control/execute vis_init.mac");
        } else {
            // Initialize before calling the gui
            G4String command = "/control/execute ";
            for (const auto& fileName : macroFiles) {
                UImanager->ApplyCommand(command + fileName);
            }
            // Now load the visualization config
            UImanager->ApplyCommand("/control/execute vis.mac");
        }
        ui->SessionStart();
        delete ui;
    } else {
        // Batch mode
        G4String command = "/control/execute ";
        for (const auto& fileName : macroFiles) {
            UImanager->ApplyCommand(command + fileName);
        }
    }

    // Job termination
    delete visManager;
    delete runManager;
}

//==============================================================================
