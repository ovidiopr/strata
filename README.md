
Strata
======

**Strata** is a MC software based on [Geant4](https://geant4.web.cern.ch/).
**Strata** was derived from **Geant4**'s example *TestEm7*, but it has been
heavily modify. The only goal of this program is academic, although we have
put a lot of effort to ensure that the results are scientifically accurate.
**Use at your own risk!**

---

## Geometry definition

The geometry consists of a multilayer (each layer composed of a homogenous
material), placed in a *world*.

Several parameters define the target geometry :

- the material of the *world* (```/strata/geometry/setWorldMat...```),
- the target temperature (```/strata/geometry/setTemperature...```),
- the transverse dimension of the multilayer (```/strata/geometry/setSizeYZ...```),
- the number of layers (```/strata/geometry/layerNumber...```),
- the thickness of each layer (```/strata/geometry/layerThickness...```),
- the material of each layer (```/strata/geometry/layerMaterial...```).

The default is just one layer, with a thickness of 20 um and composed of
silica (amorphous SiO2).

In addition, a transverse uniform magnetic field can be applied
(```/strata/geometry/setField...```).

The default geometry is constructed in DetectorConstruction class,
but all of the above parameters can be changed interactively via
the commands defined in the DetectorMessenger class.

The size and positions of several test-volumes (**voxels**) can also be
defined via UI commands : ```/strata/geometry/voxel...```

---

## Physics list

Physics lists can be **local** or from G4 kernel *physics_lists*
subdirectory.

Local physics lists:

1. **local**: Standard EM physics with current 'best' options setting.
   these options are explicited in PhysListEmStandard.
2. **standardSS**: Standard EM physics with single Coulomb scattering
   instead of multiple scattering.
3. **standardNR**: Standard EM physics with single Coulomb scattering
   process G4ScreenedNuclearRecoil instead of the multiple scattering
   for ions with energy less than 100 MeV/nucleon; the new process was
   developed by M.H. Mendenhall and R.A. Weller from Vanderbuilt University
   and published in [NIMB 227 (2005) 420-430](https://dx.doi.org/10.1016/j.nimb.2004.08.014).

From *geant4/source/physics_lists/builders*:

1. **standard_opt0**: Recommended standard EM physics for LHC.
2. **standard_opt1**: Best CPU performance standard physics for LHC.
3. **standard_opt2**: Similar fast simulation.
4. **standard_opt3**: Best standard EM options - analog to **local** above.
5. **standard_opt4**: Best current advanced EM options standard + lowenergy.
6. **standardATIMA**: ATIMA (ATomic Interaction with MAtter) energy-loss model.
7. **ionGasModels**: Ion gas models.
8. **standardWVI**: Standard EM physics and WentzelVI multiple scattering.
9. **standardGS**: Standard EM physics and Goudsmit-Saunderson multiple scattering.
10. **standard5D**: Five-dimensional (5D) Bethe-Heitler Model.
11. **livermore**: Low-energy EM physics using Livermore data.
12. **penelope**: Low-energy EM physics implementing Penelope models.
13. **lowenergy**: Low-energy EM physics implementing experimental low-energy models.
14. **dna**: Default Geant4-DNA-based physics list.
15. **dna_opt2**: Electrons interactions **up to 1 MeV**, as well as all other
    particle interactions.
16. **dna_opt4**: Electron elastic and inelastic models by D. Emfietzoglou,
    I. Kyriakou, S. Incerti, **up to 10 keV**.
17. **dna_opt6**: CPA100 electron elastic and inelastic models by M. C. Bordage,
    M. Terrissol, S. Incerti, **up to 255 keV**.

Physic lists for hadron therapy:

1. **hadron_therapy**: Electromagnetic + hadronic models.
2. **hadron_therapy_hp**: Electromagnetic + hadronic models (High Precision).

**Decay** and **StepMax** processes are added to each list.

Optional components can be added:

- **rad_decay**: Hadron radioactive decay physics.
- **binary_cascade**: Ion binary cascade physics.
- **extra_physics**: Extra physics.
- **elastic**: Elastic scattering of hadrons (Chips Elastic Model).
- **elastic_hp**: Elastic scattering of hadrons (High Precision).
- **DElastic**: Elastic scattering of hadrons (Hadron Elastic Diffuse Model).
- **HElastic**: Elastic scattering of hadrons (Glauber Hadron Elastic Model).
- **binary**: QBBC configuration of hadron/ion inelastic models.
- **binary_ion**: Binary ion inelastic models.
- **gamma_nuc**: Gamma- and electro-nuclear processes.
- **stopping**: Stopping processes.
- **qgsp_bic**: Quark-Gluon-String model + Binary.
- **qgsp_bic_hp**: Quark-Gluon-String model + Binary (High Precision).
- **neutron_tracking**: Neutron tracking physics.

Physics lists and options can be (re)set with UI commands, e.g.:

```
> /strata/physics/addPhysics local
```

Please, notice that options set through **G4EmProcessOptions** are global, eg
for all particle types. In G4 builders, it is shown how to set options per
particle type.

---

## An event: The primary generator

The primary kinematic consists of a single particle which hits the
block perpendicular to the input face. The type of the particle
and its energy are set in the PrimaryGeneratorAction class, and can
changed via the G4 build-in commands of **G4ParticleGun** class (see
the macros provided with this example).

*The default is a 1 MeV proton.*

In addition one can define randomly the impact point of the incident
particle. The corresponding interactive command is built in
PrimaryGeneratorMessenger class.

A RUN is a set of events.

---

## Dose in *test volumes*

The energy deposited in the test-volumes (voxels) defined in
DetectorConstruction are printed at RunAction::EndOfRunAction(), both in MeV and gray.

---

## Visualization

The Visualization Manager is set in the main () (see Strata.cc).
The initialisation of the drawing is done via the command:
```
> /control/execute init_vis.mac
```

The detector has a default view which is a longitudinal view of the box.

The tracks are drawn at the end of event, and erased at the end of run.
Optionally one can choose to draw all particles, only the charged one,
or none. This command is defined in EventActionMessenger class.

---

## How to start?

- Execute Test  in **batch mode** from macro files:
```
  % Strata proton.mac
```

- Execute Test  in **interactive mode** with visualization
```
  % Strata
  ....
  Idle> type your commands
  ....
  Idle> exit
```

---

## Histograms

Strata contains several built-in 1D histograms, which are managed by
G4AnalysisManager class and its Messenger. All the histograms are
deactivated by default, and can be individually activated with the
command:
```
/analysis/h1/set id nbBins valMin valMax unit
```
where unit is the desired unit for the histogram (MeV or keV, deg
or mrad, etc...). For more details, see the macros *xxxx.mac*.

- **1**: Ionizing energy deposited along the trajectory of
         the incident particle (Se)
- **2**: Non-ionizing energy deposited along the trajectory of
         the incident particle (Sn)
- **3**: Projectile range
- **10**: Energy deposited in the target (dN/dE)
- **11**: Energy of charged secondaries at creation
- **12**: Energy of neutral secondaries at creation
- **13**: Energy of charged at creation (log10(Ekin))
- **14**: Energy of neutral at creation (log10(Ekin))
- **15**: X vertex of charged secondaries (all)
- **16**: X vertex of charged secondaries (not absorbed)
- **20**: Kinetic energy at exit of target (transmitted, charged)
- **21**: Energy fluence, dE/dOmega (transmitted, charged)
- **22**: Space angle, dN/dOmega (transmitted, charged)
- **23**: Projected angle at exit of target (transmitted, charged)
- **24**: Projected position at exit of target (transmitted, charged)
- **25**: Radius at exit of target (transmitted, charged)
- **30**: Kinetic energy at exit of target (transmitted, neutral)
- **31**: Energy fluence, dE/dOmega (transmitted, neutral)
- **32**: Space angle, dN/dOmega (transmitted, neutral)
- **33**: Projected angle at exit of target (transmitted, neutral)
- **40**: Kinetic energy at exit of target (reflected, charged)
- **41**: Energy fluence, dE/dOmega (reflected, charged)
- **42**: Space angle, dN/dOmega (reflected, charged)
- **43**: Projected angle at exit of target (reflected, charged)
- **50**: Kinetic energy at exit of target (reflected, neutral)
- **51**: Energy fluence, dE/dOmega (reflected, neutral)
- **52**: Space angle, dN/dOmega (reflected, neutral)
- **53**: Projected angle at exit of target (reflected, neutral)
- **60**: Energy of Auger e- at creation
- **61**: Energy of fluorescence gamma at creation
- **62**: Energy of Auger e- at creation (log scale)
- **63**: Energy of fluorescence gamma at creation (log scale)
- **64**: Energy of PIXE Auger e- at creation
- **65**: Energy of PIXE gamma at creation
- **66**: Energy of PIXE Auger e- at creation (log scale)
- **67**: Energy of PIXE gamma at creation (log scale)
- **68**: Energy of G4DNA Auger e- at creation
- **69**: Energy of G4DNA gamma at creation
- **70**: Energy of G4DNA Auger e- at creation (log scale)
- **71**: Energy of G4DNA gamma at creation (log scale)

One can control the name of the histograms file with the command:
```
/analysis/setFileName name (default strata)
```

It is possible to choose the format of the histogram file : root (default),
xml, csv, by using namespace in *HistoManager.hh*. It is also possible to print selected histograms on an ascii file:
```
/analysis/h1/setAscii id
```
All selected histograms will be written on a file name.ascii  (default strata)
