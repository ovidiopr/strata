//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file Strata/src/PrimaryGeneratorMessenger.cc
/// \brief Implementation of the PrimaryGeneratorMessenger class
//
//
//==============================================================================
//==============================================================================

#include "PrimaryGeneratorMessenger.hh"
#include "PrimaryGeneratorAction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"

//==============================================================================

PrimaryGeneratorMessenger::PrimaryGeneratorMessenger(PrimaryGeneratorAction* Gun)
        : G4UImessenger(), fAction(Gun),
          fGunDir(nullptr),
          fRndmCmd(nullptr) {
    fGunDir = new G4UIdirectory("/strata/gun/");
    fGunDir->SetGuidance("Gun control.");

    fDefaultCmd = new G4UIcmdWithoutParameter("/strata/gun/setDefault", this);
    fDefaultCmd->SetGuidance("Set/reset the kinematic defined in PrimaryGenerator.");
    fDefaultCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

    fRndmCmd = new G4UIcmdWithADoubleAndUnit("/strata/gun/rndBeam", this);
    fRndmCmd->SetGuidance("Random lateral extension on the beam.");
    fRndmCmd->SetParameterName("rBeam", false);
    fRndmCmd->SetRange("rBeam>=0.");
    fRndmCmd->SetUnitCategory("Length");
    fRndmCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
}

//==============================================================================

PrimaryGeneratorMessenger::~PrimaryGeneratorMessenger() {
    delete fDefaultCmd;
    delete fRndmCmd;
    delete fGunDir;
}

//==============================================================================

void PrimaryGeneratorMessenger::SetNewValue(G4UIcommand* command, G4String newValue) {
    if (command == fDefaultCmd)
    {fAction->SetDefaultKinematic();}
    if (command == fRndmCmd) { fAction->SetRndmBeam(G4UIcmdWithADoubleAndUnit::GetNewDoubleValue(newValue)); }
}

//==============================================================================

