//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file Strata/src/SteppingAction.cc
/// \brief Implementation of the SteppingAction class
//
//
//==============================================================================
//==============================================================================

#include "SteppingAction.hh"

#include "DetectorConstruction.hh"
#include "RunAction.hh"
#include "EventAction.hh"
#include "HistoManager.hh"
#include "Run.hh"

#include "G4Step.hh"
#include "G4RunManager.hh"

//==============================================================================

SteppingAction::SteppingAction(DetectorConstruction* det, EventAction* EvAct, RunAction* RunAct) :
        G4UserSteppingAction(),
        fDetector(det),
        fEventAction(EvAct),
        fRunAction(RunAct) { }

//==============================================================================

SteppingAction::~SteppingAction() = default;

//==============================================================================

void SteppingAction::UserSteppingAction(const G4Step* aStep) {
//    if (aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume()
//        != fDetector->GetAbsorber())
//        return;

    // Save tracks to files
    if (fRunAction->GetSavePrimaries() || fRunAction->GetSaveCascades()) {
        fRunAction->WriteToFiles(aStep);
    }

    G4double Edep = aStep->GetTotalEnergyDeposit();
    if (Edep <= 0.) return;

    G4double Eniel = aStep->GetNonIonizingEnergyDeposit();

    Run* run = static_cast<Run*>(
            G4RunManager::GetRunManager()->GetNonConstCurrentRun());

    fEventAction->AddEnergy(Edep);
    fEventAction->AddNIELEnergy(Eniel);

    G4double charge = aStep->GetTrack()->GetDefinition()->GetPDGCharge();
    if (charge != 0.) {
        fEventAction->AddTrakLenCharg(aStep->GetStepLength());
        fEventAction->CountStepsCharg();
    } else {
        fEventAction->AddTrakLenNeutr(aStep->GetStepLength());
        fEventAction->CountStepsNeutr();
    }

    G4StepPoint* prePoint = aStep->GetPreStepPoint();
    G4StepPoint* postPoint = aStep->GetPostStepPoint();

    G4int copyNb = prePoint->GetTouchableHandle()->GetCopyNumber();
    if (copyNb > 0) { run->FillVoxelEdep(copyNb - 1, Edep); }

    if (aStep->GetTrack()->GetTrackID() == 1) {
        run->AddPrimaryStep();
    }

    //Bragg curve (separated in ionizing and non-ionizing fractions)
    G4double xmax = fDetector->GetMultiLayerSizeX();

    G4double x1 = prePoint->GetPosition().x() + xmax*0.5;
    G4double x2 = postPoint->GetPosition().x() + xmax*0.5;
    if (x1 >= 0.0 && x2 <= xmax) {
        G4double x = x1 + G4UniformRand()*(x2 - x1);
        if (aStep->GetTrack()->GetDefinition()->GetPDGCharge() == 0.) x = x2;
        G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
        // The ionizing fraction of the energy deposited
        analysisManager->FillH1(1, x, Edep - Eniel);
        // The non-ionizing fraction of the energy deposited
        analysisManager->FillH1(2, x, Eniel);
    }
}

//==============================================================================
