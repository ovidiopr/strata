//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file Strata/src/RunAction.cc
/// \brief Implementation of the RunAction class
//
//
//==============================================================================
//==============================================================================

#include "RunAction.hh"
#include "RunActionMessenger.hh"

#include "DetectorConstruction.hh"
#include "PrimaryGeneratorAction.hh"
#include "HistoManager.hh"
#include "Run.hh"

#include "G4Run.hh"
#include "G4UnitsTable.hh"
#include "G4EmCalculator.hh"
#include "G4VProcess.hh"

#include "Randomize.hh"
#include "G4SystemOfUnits.hh"
#include <iomanip>

//==============================================================================

RunAction::RunAction(DetectorConstruction* det, PrimaryGeneratorAction* kin)
        : G4UserRunAction(), fDetector(det), fPrimary(kin),
          fRun(nullptr), fHistoManager(nullptr),
          fSavePrim(false),
          fSaveCasc(false),
          fPrimName("primaries.txt"),
          fCascName("cascades.txt"),
          fEventNum(0) {

    // Book predefined histograms
    fHistoManager = new HistoManager();

    // Create messenger
    fRunMessenger = new RunActionMessenger(this);
}

//==============================================================================

RunAction::~RunAction() {
    delete fHistoManager;

    if (fPrimFile.is_open()) fPrimFile.close();
    if (fCascFile.is_open()) fCascFile.close();

    delete fRunMessenger;
}

//==============================================================================

G4Run* RunAction::GenerateRun() {
    fRun = new Run(fDetector);
    return fRun;
}

//==============================================================================

void RunAction::BeginOfRunAction(const G4Run*) {
    // Show Rndm status
    if (isMaster) G4Random::showEngineStatus();

    // Keep run condition
    if (fPrimary) {
        G4ParticleDefinition* particle
                = fPrimary->GetParticleGun()->GetParticleDefinition();
        G4double energy = fPrimary->GetParticleGun()->GetParticleEnergy();
        fRun->SetPrimary(particle, energy);
    }

    // Histograms
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    if (analysisManager->IsActive()) {
        analysisManager->OpenFile();
    }

    // Activate saving to file
    if (fSavePrim) {
        fPrimFile.open(fPrimName, std::ios::out);    //ar
        fPrimFile << "#ev. " << "stp. " << "id " << "idpapa " << "name "
                  << "time " << "x " << "y " << "z " << "px "
                  << "py " << "pz " << "Ek" << " proc\n";
    }

    if (fSaveCasc) {
        fCascFile.open(fCascName, std::ios::out);    //ar
        fCascFile << "#ev. " << "stp. " << "id " << "idpapa " << "name "
                  << "time " << "x " << "y " << "z " << "px "
                  << "py " << "pz " << "Ek" << " proc\n";
    }
}

//==============================================================================

void RunAction::EndOfRunAction(const G4Run*) {
    // Print Run summary and finish saving to file
    if (isMaster) fRun->EndOfRun();

    // Save histograms
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    if (analysisManager->IsActive()) {
        analysisManager->Write();
        analysisManager->CloseFile();
    }

    // Show Rndm status
    if (isMaster) G4Random::showEngineStatus();

    // Close primaries and cascades files
    if (fPrimFile.is_open()) fPrimFile.close();
    if (fCascFile.is_open()) fCascFile.close();
}

//==============================================================================

void RunAction::WriteToFiles(const G4Step* step) {
    const G4Track* track = step->GetTrack();
    if (!track) {
        G4cout << "No track - Exiting...\n";
        return;
    }
    const int nstep = track->GetCurrentStepNumber();
    const int id = track->GetTrackID();
    const int parentID = track->GetParentID();
    const G4String particleName = track->GetDynamicParticle()->GetParticleDefinition()->GetParticleName();
    const G4VProcess* process = track->GetCreatorProcess();
    G4String processName;
    if (process != nullptr) {
        processName = process->GetProcessName();
    } else processName = "Primary";

    if (id == 1 && nstep == 1) fEventNum++;   //ar here we get the event number

    // get initial and final points in a step of the particle between two interactions
    const G4StepPoint* pnt1 = step->GetPreStepPoint();
    const G4StepPoint* pnt2 = step->GetPostStepPoint();

    // properties of particle at G4StepPoint pnt1 (only for step 1 to get first point of particle trajectory)
    if (nstep == 1) {
        if (id == 1) { //ar primary
            if (fSavePrim) {
                fPrimFile << fEventNum << " " << nstep - 1 << " " << id << " "
                          << parentID << " " << particleName << " ";
            }
        } else { //ar cascades
            if (fSaveCasc) {
                fCascFile << fEventNum << " " << nstep - 1 << " " << id << " "
                          << parentID << " " << particleName << " ";
            }
        }
        const G4ThreeVector& pos = pnt1->GetPosition();
        const G4double dt_global = pnt1->GetGlobalTime();
        const G4ThreeVector mom = pnt1->GetMomentum();
        const G4double ekin = pnt1->GetKineticEnergy();
        //const G4double ene = pnt1->GetTotalEnergy();

        if (id == 1) { //ar primary
            if (fSavePrim) {}
            fPrimFile << dt_global << " "
                      << pos[0] << " " << pos[1] << " " << pos[2] << " "
                      << mom[0] << " " << mom[1] << " " << mom[2] << " "
                      << ekin << " " << processName << "\n";
        } else {   //ar cascades
            if (fSaveCasc) {}
            fCascFile << dt_global << " "
                      << pos[0] << " " << pos[1] << " " << pos[2] << " "
                      << mom[0] << " " << mom[1] << " " << mom[2] << " "
                      << ekin << " " << processName << "\n";
        }
    }

    // properties of particle at G4StepPoint pnt2
    if (pnt2) {
        if (id == 1) { //ar primary
            if (fSavePrim) {
                fPrimFile << fEventNum << " " << nstep << " " << id << " "
                          << parentID << " " << particleName << " ";
            }
        } else { //ar cascades
            if (fSaveCasc) {
                fCascFile << fEventNum << " " << nstep << " " << id << " "
                          << parentID << " " << particleName << " ";
            }
        }
        const G4ThreeVector& pos = pnt2->GetPosition();
        const G4double dt_global = pnt2->GetGlobalTime();
        const G4ThreeVector mom = pnt2->GetMomentum();
        const G4double ekin = pnt2->GetKineticEnergy();
        //const G4double ene = pnt2->GetTotalEnergy();

        if (id == 1) { //ar primary
            if (fSavePrim) {
                fPrimFile << dt_global << " "
                          << pos[0] << " " << pos[1] << " " << pos[2] << " "
                          << mom[0] << " " << mom[1] << " " << mom[2] << " "
                          << ekin << " " << processName << "\n";
            }
        } else {   //ar cascades
            if (fSaveCasc) {
                fCascFile << dt_global << " "
                          << pos[0] << " " << pos[1] << " " << pos[2] << " "
                          << mom[0] << " " << mom[1] << " " << mom[2] << " "
                          << ekin << " " << processName << "\n";
            }
        }
    } else { //ar in case some error arises
        G4cout << "Error saving track info!!\n";
    }
}

//==============================================================================

void RunAction::SetSavePrimaries(const G4bool value) {
    G4cout << "Run::SetSavePrimaries" << value << fSavePrim << G4endl;
    if (value != fSavePrim) {
        fSavePrim = value;
    }
}

//==============================================================================

void RunAction::SetSaveCascades(const G4bool value) {
    if (value != fSaveCasc) {
        fSaveCasc = value;
    }
}

//==============================================================================

void RunAction::SetPrimariesName(const G4String& value) {
    if (value != fPrimName) {
        fPrimName = value;
    }
}

//==============================================================================

void RunAction::SetCascadesName(const G4String& value) {
    if (value != fCascName) {
        fCascName = value;
    }
}

//==============================================================================
